<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wptest');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'myhjk');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Jn%sM2CHomuU1rEa,x{M?g9n<$cNN,F^Qxj{X}SwBX*T,O11.Z-8YUj`_>;t-d=#');
define('SECURE_AUTH_KEY',  '#qG,3^)XIN+d4UxqYgnpp6jc[ZOv0wzYJmC(_|eG!nrAW97O1hJ^hkmF$Yux2h!G');
define('LOGGED_IN_KEY',    '>I/o+3<UI@=.2uEWS<tzg,%=r/:VIGO}uC9L5M=AOaG*Pp@h-uVzK{Ao-^U]-Y?4');
define('NONCE_KEY',        '6AIKD4/ER*;WD~A.QkV<a+S}lp~nVNOTblb).[Z1cu#@$Y/ZZFftx@lVT)4uF.B_');
define('AUTH_SALT',        'I?&,rr^IGB]Uvbx(:.#|Is#2R+;T,wt|x.[(NC y}Saa:mw6W+92;n,l6Z |l+%W');
define('SECURE_AUTH_SALT', 'P#[2+sdx!4Hf(CmpvfuE>q&pasR%$PDGvx)q-iz%6y{V4Yi%kDA*vnHk)TGb)]_q');
define('LOGGED_IN_SALT',   'ArLY4&LNgn=Rs L>p<#AL&d=s[t8bv9rw4PFo*40MMky]z4,KF`<N+8R^jNKQm;K');
define('NONCE_SALT',       '._5bFcHlrXx0Idsh8YZuCsyNY.sxG5GYI%JyjGEQZm^+szQrE%Yf8)Ts6+heflU;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
